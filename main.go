package main

import (
	"gitlab.com/plantd/plantcli/cmd"
)

func main() {
	cmd.Execute()
}
