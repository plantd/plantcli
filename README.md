# Plantd CLI

[WIP] This is in alpha state and a lot of what's given here isn't yet
implemented.

## Install

```sh
go get -u gitlab.com/plantd/plantcli/...
```

This will place `plantcli` into `$GOPATH/bin`. If left there you would need to
replace calls to `plant` given here with `plantcli`, or just copy it somewhere
else that's in your `$PATH` and give it the name `plant`.

## Building

```sh
go build -o target/plant
```

### Cross-compiling for Windows

[FIXME] this compiles but doesn't seem to work for all commands.

Compiling requires the use of `CGO_ENABLED` to be set, which requires a
compiler to be installed that can handle this. On `ArchLinux` this meant
installing `mingw-w64-gcc` and `mingw-w64-zeromq` from the Arch user
repositories.

```sh
GOOS=windows GOARCH=amd64 CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc \
  go build -o target/plant.exe
```

## Commands

### `plant`

#### `create`

[TODO] Provide an interface and implement different languages as plugins.

##### Example

_unimplemented_

```sh
cat examples/test.json | plant create \
  --template-path ~/.templates/plantd/python
```

#### `configuration`

```sh
# ...
```

#### `job`

```sh
plant job -e "tcp://10.0.16.101:5556" \
  -m acquire-gphoto \
  -n start-acquiring \
  -p "interval=10,count=6"
```

#### `module`

```sh
# ...
```

#### Notes

These are just notes, non of this works yet.

```sh
plant network --info
org
└── plantd
    ├── foo
    │   ├── FooModule
    │   └── FooModule
    └── bar
        ├── BarModule
        └── BarModule
```

```sh
# get backplane configuration
plant configuration org.plantd
# get network broker configuration
plant configuration org.plantd.foo
# get network module configuration
plant configuration org.plantd.foo.FooModule
```

```sh
plant job
plant job --all
plant job --active
plant job cancel
plant job submit
```

```sh
plant module org.plantd.foo.FooModule
plant module --all org.plantd.foo
```

```sh
# list available properties (?)
plant property --list org.plantd.foo.FooModule
plant property org.plantd.foo.FooModule:prop
plant property org.plantd.foo.FooModule:prop=value
# get all properties
plant property org.plantd.foo.FooModule:*
plant property org.plantd.foo.FooModule:_='{"prop1":"value", "prop2":"value"}'
```

```sh
plant event --available org.plantd.foo.FooModule
plant event --listen org.plant.foo
plant event submit --data='{"id":3000}' org.plantd.foo
```

```sh
plant metric --listen --filter FooModule org.plantd.foo
plant metric send --data='{"...":"..."}' org.plantd.foo
```
