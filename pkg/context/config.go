package context

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

type server struct {
	Endpoint string `mapstructure:"endpoint"`
}

type Config struct {
	Server server `mapstructure:"server"`
}

func LoadConfig(file string) (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()

	if file == "" {
		file = os.Getenv("PLANT_CONFIG")
	}

	if file == "" {
		config.SetConfigName("plant")
		config.AddConfigPath("/etc/plantd")
		config.AddConfigPath(".")
		config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
	} else {
		// Use config file from the flag
		base := filepath.Base(file)
		if strings.HasSuffix(base, "yaml") ||
			strings.HasSuffix(base, "json") ||
			strings.HasSuffix(base, "hcl") ||
			strings.HasSuffix(base, "toml") ||
			strings.HasSuffix(base, "conf") {
			// strip the file type for viper
			parts := strings.Split(filepath.Base(file), ".")
			base = strings.Join(parts[:len(parts)-1], ".")
		}
		config.SetConfigName(base)
		config.AddConfigPath(filepath.Dir(file))
	}

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANT")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}
