package cmd

import (
	"encoding/json"
	"log"
	"os"
	"text/template"

	"github.com/spf13/cobra"
)

// project:
// - name (scope-module)
// - short-name (module)
// - version
// - service (org.plantd.scope.Module)
// - language
// - author:
//   - name
//   - email
// - git:
//	 - repo
//	 - issues

type project struct {
	Name      string `json: "name"`
	ShortName string `json: "short-name"`
	Version   string `json: "version"`
	Service   string `json: "service"`
	Language  string `json: "language"`
}

type author struct {
	Name  string `json: "name"`
	Email string `json: "email"`
}

type git struct {
	Repo   string `json: "repo"`
	Issues string `json: "issues"`
}

type Module struct {
	Project project `json: "project"`
	Author  author  `json: "author"`
	Git     git     `json: "git"`
}

// TODO: put this into a plugin or a subcommand by language?

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create plantd services from templates",
	Run: func(cmd *cobra.Command, args []string) {

		var module Module
		if err := json.NewDecoder(os.Stdin).Decode(&module); err != nil {
			log.Fatal(err)
		}

		templatePath := "data/templates/python/"

		// TODO: generate automatically
		paths := map[string]string{
			"readme": templatePath + "README.md.tmpl",
			"setupp": templatePath + "setup.py.tmpl",
			"setupc": templatePath + "setup.cfg.tmpl",
			"prjbin": templatePath + "bin/project_name.tmpl",
			"pminit": templatePath + "project_short_name/__init__.py.tmpl",
			"pmmodp": templatePath + "project_short_name/module.py.tmpl",
			"pmjobp": templatePath + "project_short_name/job.py.tmpl",
		}

		// XXX: just for testing
		keys := make([]string, 0, len(paths))
		values := make([]string, 0, len(paths))

		for k, v := range paths {
			keys = append(keys, k)
			values = append(values, v)
		}

		//for k, v := range paths {
		t := template.Must(template.ParseFiles(values...))
		if err := t.Execute(os.Stdout, module); err != nil {
			panic(err)
		}
		//}

		//t, _ := findAndParseTemplates(templatePath, nil)
		//_ := t.ExecuteTemplate(w, )
	},
}

// TODO: create directory structure
// TODO: copy non template files to new project

// TODO: read rootDir from optional path, or system install location
// TODO: run template.ExecuteTemplate(w, "dir/file.tmpl", nil)
/*
 *func findAndParseTemplates(rootDir string, funcMap template.FuncMap) (*template.Template, error) {
 *    cleanRoot := filepath.Clean(rootDir)
 *    pfx := len(cleanRoot) + 1
 *    root := template.New("")
 *
 *    err := filepath.Walk(cleanRoot, func(path string, info os.FileInfo, e1 error) error {
 *        if !info.IsDir() && strings.HasSuffix(path, ".tmpl") {
 *            if e1 != nil {
 *                return e1
 *            }
 *
 *            b, e2 := ioutil.ReadFile(path)
 *            if e2 != nil {
 *                return e2
 *            }
 *
 *            name := path[pfx:]
 *            t := root.New(name).Funcs(funcMap)
 *            t, e2 = t.Parse(string(b))
 *            if e2 != nil {
 *                return e2
 *            }
 *        }
 *
 *        return nil
 *    })
 *
 *    return root, err
 *}
 */
