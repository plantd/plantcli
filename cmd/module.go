package cmd

import (
	"fmt"
	"log"

	plantd "gitlab.com/plantd/go-plantd/service"

	"github.com/spf13/cobra"
)

var (
	moduleCmd = &cobra.Command{
		Use:   "module",
		Short: "Perform module functions with plantd",
		Args:  cobra.ExactArgs(1),
		Run:   module,
	}
)

func module(cmd *cobra.Command, args []string) {
	// TODO: do client loading somewhere else
	client, err := plantd.NewClient(config.Server.Endpoint)
	if err != nil {
		log.Fatal(err)
	}

	id := args[0]
	response, err := client.GetModule(id)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(response.String())
}
