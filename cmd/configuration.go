package cmd

import (
	"bytes"
	//"fmt"
	"log"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"gitlab.com/plantd/go-zapi/mdp"

	"github.com/golang/protobuf/jsonpb"
	"github.com/spf13/cobra"
)

var (
	//id string

	configurationCmd = &cobra.Command{
		Use:   "configuration",
		Short: "Perform configuration functions with plantd",
		Run:   configuration,
	}
)

/*
 *func init() {
 *  configurationCmd.PersistentFlags().StringVarP(&id, "id", "i", "", "service ID as reverse path")
 *}
 */

func configuration(cmd *cobra.Command, args []string) {
	client, err := mdp.NewClient(config.Server.Endpoint)
	if err != nil {
		log.Fatal(err)
	}

	id := "test"

	request := &pb.ConfigurationRequest{
		Id: id,
	}

	req := make([]string, 2)
	req[0] = "configuration"

	response := &pb.ConfigurationResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, request); err != nil {
		log.Fatal(err)
	}

	// Send the message
	req[1] = b.String()
	_ = client.Send(id, req...)
	// Wait for a reply
	reply, err := client.Recv()
	if err != nil {
		log.Fatal(err)
	}

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		log.Fatal("didn't receive expected response")
	} else if len(reply) > 1 {
		// only handle first response
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[1])), response); err != nil {
		log.Fatal(err)
	}

	//fmt.Println("retrieved configuration with ID " + response.Id)
}
