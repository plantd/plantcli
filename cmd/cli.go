package cmd

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/plantd/plantcli/pkg/context"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile  string
	config   *context.Config
	endpoint string
	Verbose  bool

	cliCmd = &cobra.Command{
		Use:   "plant",
		Short: "Application to control plantd services",
		Long:  `A control utility for interacting with plantd services.`,
	}
)

func Execute() {
	if err := cliCmd.Execute(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	addCommands()

	// Setup command flags
	cliCmd.PersistentFlags().StringVar(
		&cfgFile,
		"config", "",
		"config file (default is $HOME/.config/plantd/plant.yaml)",
	)
	cliCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")

	viper.BindPFlag("verbose", cliCmd.PersistentFlags().Lookup("verbose"))
	viper.SetDefault("verbose", false)
}

func addCommands() {
	cliCmd.AddCommand(configurationCmd)
	cliCmd.AddCommand(createCmd)
	cliCmd.AddCommand(jobCmd)
	cliCmd.AddCommand(moduleCmd)

	// Miscellaneous commands
	cliCmd.AddCommand(versionCmd)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	fmt.Println("Attempt to load:", cfgFile)
	config, err := context.LoadConfig(cfgFile)
	if err != nil {
		fmt.Errorf("Fatal error reading config file: %s \n", err)
	}

	if config == nil {
		fmt.Errorf("Something wrong happened loading configuration")
	}

	endpoint = config.Server.Endpoint
	fmt.Println("Using endpoint:", config.Server.Endpoint)
}
